# amazonPiSend

Send data to Amazon IoT Core from the Pi Zero. This assumes that you have already run the example pubSub scripts that come with the connection package and have them working.

## Basic Instructions

On your Pi Zero, install AWSIoTPythonSDK:
```
pip3 install AWSIoTPythonSDK
```
Clone this repository:
```
git clone https://gitlab.com/2020-cptr-487-iot/amazonpisend.git
```
The master branch version (which is the default as cloned) just sends numbers and is good for testing purposes. To send lux data from your WavaShare SenseHat (B), run the following command to checkout the `sensor` branch:
```
git checkout sensor
```

### Host/endpoint
Go to -> AWS IoT -> Settings
It should list your custom endpoint.

### Editing policy 
In order to publish or subscribe to topics, they have to be allowed in your thing's policy which is attached to its certificate.

Change the policy as follows:
- AWS IoT Core -> Manage -> Things -> Click on your Thing
- Click on Security
- Underneath Creat certificate should be a button with a bunch of randomish characters. Click this button.
- Click Policies on the left
- Click the policy that shows up. Mine is the name of my thing-Policy
This document shows what your Thing is allowed to do. Which topics it can subscribe to and which topics it can publish to.
- Click Edit policy document
- Copy the policy document from the repository into the policy
- Update the arn in the policy to match the arn for your device as shown at the top of the policy page. If you are in the Ohio region, then you probably just have to change the number.
- Save new version of the policy


### Running the script
A typical initial run of the script is as follows:
```python3 pubSub.py -e YourCustomEndpoint -r ../root-CA.crt -c ../yourDeviceName.cert.pem -k ../yourDeviceName.private.key```

Running with a client id and using device name as topic base (be sure to change DrSethPiZero to the name of your device):
```python3 pubSub.py -e YourCustomEndpoint -r ../root-CA.crt -c ../DrSethPiZero.cert.pem -k ../DrSethPiZero.private.key -id DrSethPiZero -t DrSethPiZero/test```

### Monitor the messages
- In the IoT Core Console click Test on the left had side
- In subscribe to topic put your topic name. If you used the above example the topic would be DrSethPiZero/test. If you are sending lux data it would be DrSethPiZero/test/lux.
- The messages should start showing up below where you entered the subscription topic.

## IoT Things Graph
Unsupported in our region (supported in US East (N. Virginia) and US West)

## Amazon Kinesis Data Firehose -> S3 -> QuickSight
Based on https://aws.amazon.com/blogs/compute/visualizing-sensor-data-in-amazon-quicksight/ starting at "Create a Kinesis Firehose delivery stream"
- Kinesis Main page -> Dashboard -> Create Delivery Stream
- But Firehose Delivery Streams are not part of the free tier


# References
- https://aws.amazon.com/blogs/big-data/build-a-visualization-and-monitoring-dashboard-for-iot-data-with-amazon-kinesis-analytics-and-amazon-quicksight/
- https://aws.amazon.com/iot-things-graph/
- https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html
- https://docs.aws.amazon.com/IAM/latest/UserGuide/getting-started_create-admin-group.html
- https://aws.amazon.com/blogs/iot/route-data-directly-from-iot-core-to-your-web-services/
- I think this is the one that will be most useful: https://aws.amazon.com/blogs/compute/visualizing-sensor-data-in-amazon-quicksight/
- https://www.hackster.io/Altaga/chemoai-d20885 uses a different device, but AWS IoT setup is similar
- Policy:
    - https://docs.aws.amazon.com/iot/latest/developerguide/create-iot-policy.html
    - https://docs.aws.amazon.com/iot/latest/developerguide/iot-policies.html
